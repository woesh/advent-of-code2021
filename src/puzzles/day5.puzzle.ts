import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { filter, Observable, of, OperatorFunction, reduce } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Puzzle(5)
export class Day5Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((v) => /(\w+),(\w+)\s+->\s+(\w+),(\w+)/gm.exec(v)),
            filter((match) => match !== null) as OperatorFunction<any, RegExpExecArray>,
            filter(([, fromX, fromY, toX, toY]) => fromX === toX || fromY === toY),
            map(([, ...coords]) => {
                const [fromX, fromY, toX, toY] = coords.map((v) => parseInt(v));
                return [
                    [fromX, fromY],
                    [toX, toY],
                ] as [Coordinate, Coordinate];
            }),
            map((coords) => sortCoordinates(coords)),
            switchMap(([from, to]: Line) => {
                const gradient = getGradient(from, to);
                const intercept = getIntercept(from, gradient);
                const coordinates: Array<Coordinate> = [];

                if (from[0] - to[0] !== 0) {
                    for (let x = from[0]; x <= to[0]; x++) {
                        const y = (gradient ?? 0) * x + intercept;
                        coordinates.push([x, y]);
                    }
                } else {
                    for (let y = from[1]; y <= to[1]; y++) {
                        const x = from[0];
                        coordinates.push([x, y]);
                    }
                }

                return of(...coordinates) as Observable<Coordinate>;
            }),
            map((coordinate: Coordinate) => coordinate.join(',')),
            reduce((acc, value) => {
                acc[value] = (acc[value] ?? 0) + 1;
                return acc;
            }, {} as any),
            map((count: object) => Object.values(count).filter((v: number) => v >= 2).length)
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((v) => /(\w+),(\w+)\s+->\s+(\w+),(\w+)/gm.exec(v)),
            filter((match) => match !== null) as OperatorFunction<any, RegExpExecArray>,
            map(([, ...coords]) => {
                const [fromX, fromY, toX, toY] = coords.map((v) => parseInt(v));
                return [
                    [fromX, fromY],
                    [toX, toY],
                ] as [Coordinate, Coordinate];
            }),
            map((coords) => sortCoordinates(coords)),
            switchMap(([from, to]: Line) => {
                const gradient = getGradient(from, to);
                const intercept = getIntercept(from, gradient);
                const coordinates: Array<Coordinate> = [];

                if (from[0] - to[0] !== 0) {
                    for (let x = from[0]; x <= to[0]; x++) {
                        const y = (gradient ?? 0) * x + intercept;
                        coordinates.push([x, y]);
                    }
                } else {
                    for (let y = from[1]; y <= to[1]; y++) {
                        const x = from[0];
                        coordinates.push([x, y]);
                    }
                }

                return of(...coordinates) as Observable<Coordinate>;
            }),
            map((coordinate: Coordinate) => coordinate.join(',')),
            reduce((acc, value) => {
                acc[value] = (acc[value] ?? 0) + 1;
                return acc;
            }, {} as any),
            map((count: object) => Object.values(count).filter((v: number) => v >= 2).length)
        );
    }
}

function getGradient(from: Coordinate, to: Coordinate): number | null {
    return to[0] !== from[0] ? (to[1] - from[1]) / (to[0] - from[0]) : null;
}

function getIntercept(from: Coordinate, gradient: number | null) {
    return gradient !== null ? from[1] - gradient * from[0] : from[0];
}

function sortCoordinates(coords: Line): Line {
    return coords.sort((a: Coordinate, b: Coordinate) => {
        const xDiff = a[0] - b[0];
        return xDiff === 0 ? a[1] - b[1] : xDiff;
    });
}

type Coordinate = [number, number];
type Line = [Coordinate, Coordinate];
