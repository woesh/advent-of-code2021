import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(3)
export class Day3Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent().pipe(
            map(([...s]) => s),
            reduce((a, c) => {
                return a.map((v, i) => (v ?? 0) + parseInt(c[i]));
            }, new Array(12).fill(0)),
            map((v) => [v.map((c: any) => (c >= 500 ? 1 : 0)), v.map((c: any) => (c < 500 ? 1 : 0))]),
            map(([gamma, epsilon]) => parseInt(gamma.join(''), 2) * parseInt(epsilon.join(''), 2))
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getLines().pipe(
            map((values) => [[...values], [...values], values[0].length] as [Array<string>, Array<string>, number]),
            map(([oxygen, co2, length]) => {
                oxygen = oxygen.reduce((acc, _, index) => {
                    return acc.length > 1 ? validBinary(acc, index, 'oxygen') : acc;
                }, oxygen);

                co2 = co2.reduce((acc, _, index) => {
                    return acc.length > 1 ? validBinary(acc, index, 'co2') : acc;
                }, co2);

                return [...oxygen, ...co2];
            }),
            map(([oxygen, c02]) => parseInt(oxygen, 2) * parseInt(c02, 2))
        );
    }
}

function validBinary(array: Array<string>, index: number, mode: 'oxygen' | 'co2') {
    const x = array.map((v) => v[index]).reduce((a, c) => a + parseInt(c), 0) / array.length;
    const isOne: number = mode === 'oxygen' ? Math.round(x) : 1 - Math.round(x);
    return array.filter((v) => parseInt(v[index]) === isOne);
}
