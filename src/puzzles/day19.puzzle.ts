import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, of, tap } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ArrayCoordinates, ThreeDimensionCoordinates } from '../utils/generic/coordinates.interface';

@Puzzle(19)
export class Day19Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getFullInput(true).pipe(
            map((a) => {
                let split = a.split(/---\s+scanner\s+(\d+)\s+---/gm),
                    scanners = [];
                while (split.length > 1) {
                    scanners.push(parseInput(split));
                }
                return scanners;
            })
            // tap(console.log)
        );
    }

    part2(): Observable<any> {
        return super.part2();
    }
}

class Scanner {
    id: string;
    reports: Array<ThreeDimensionCoordinates> = [];
    hashes: Array<BeaconHash> = [];

    constructor(id: string) {
        this.id = id;
    }

    addReport(x: number, y: number, z: number) {
        this.reports.push({ x, y, z });
        this.hashes.push(hash({ x, y, z }));
    }
}

function parseInput(splitInput: Array<string>): Scanner {
    const [id, locations] = splitInput.splice(-2, 2);
    const scanner = new Scanner(id);
    locations
        .split(/\r?\n/gm)
        .filter((n) => n)
        .map((s) => s.split(',').map((s) => parseInt(s)) as ArrayCoordinates<3>)
        .forEach((ac: ArrayCoordinates<3>) => scanner.addReport(...ac));
    return scanner;
}

function intersection(setA: Set<any>, setB: Set<any>) {
    return new Set([...setA].filter((a) => setB.has(a)));
}

function hash({ x, y, z }: ThreeDimensionCoordinates) {
    return new BeaconHash({ x, y, z });
}

class BeaconHash {
    distance: number;
    minOffset: number;
    maxOffset: number;

    constructor({ x, y, z }: ThreeDimensionCoordinates) {
        this.distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        this.minOffset = Math.min(Math.abs(x), Math.abs(y), Math.abs(z));
        this.maxOffset = Math.max(Math.abs(x), Math.abs(y), Math.abs(z));
    }
}
