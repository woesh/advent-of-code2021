import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, of, reduce } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Matrix } from '../utils/generic/matrix.class';

@Puzzle(4)
export class Day4Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ splitChar: /\r?\n\s*\r?\n/gm }).pipe(
            map(([input, ...cards]) => [
                input.split(',').map((s) => parseInt(s)),
                cards.map((s) => {
                    return createCard(
                        s
                            .split(/\W\s*/gm)
                            .filter((s) => !isNaN(parseInt(s)))
                            .map((s) => parseInt(s))
                    );
                }),
            ]),
            switchMap(([input, cards]) => {
                return of(...input).pipe(
                    reduce((acc, draw) => {
                        if (acc === -1) {
                            return hasBingo(cards as Array<BingoCard>, draw as number);
                        }
                        return acc;
                    }, -1)
                );
            })
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ splitChar: /\r?\n\s*\r?\n/gm }).pipe(
            map(([input, ...cards]) => [
                input.split(',').map((s) => parseInt(s)),
                cards.map((s) => {
                    return createCard(
                        s
                            .split(/\W\s*/gm)
                            .filter((s) => !isNaN(parseInt(s)))
                            .map((s) => parseInt(s))
                    );
                }),
            ]),
            switchMap(([input, cards]) => {
                return of(...input).pipe(
                    reduce((acc, draw) => {
                        const score = (cards as Array<BingoCard>)
                            .filter((c) => !c.hasBingo)
                            .reduce((score, card) => {
                                card.drawNumber(draw as number);
                                if (card.verifyBingo(draw as number)) {
                                    return card.getScore(draw as number);
                                }
                                return score;
                            }, -1);
                        if (score > 0) {
                            return score;
                        }
                        return acc;
                    }, -1)
                );
            })
        );
    }
}

function createCard(values: Array<number>): BingoCard {
    const bingoCard = new BingoCard();

    values.forEach((v) => bingoCard.addValue(v));

    return bingoCard;
}

function hasBingo(cards: Array<BingoCard>, draw: number): number {
    return cards.reduce((score, card) => {
        card.drawNumber(draw);
        if (card.verifyBingo(draw)) {
            return card.getScore(draw);
        }
        return score;
    }, -1);
}

class BingoCard extends Matrix<{ value: number; drawn: boolean }> {
    public hasBingo: boolean = false;

    constructor() {
        super(5, 5);
    }

    addValue(value: number) {
        super.add({ value, drawn: false });
    }

    drawNumber(value: number): void {
        const position = this.position({ value });
        if (position !== null) {
            const { x, y } = position;
            this.set(x, y, { value, drawn: true });
        }
    }

    verifyBingo(value: number) {
        const position = this.position({ value });
        if (position !== null) {
            const { x, y } = position;
            return this.verifyBingoRow(y) || this.verifyBingoColumn(x);
        }
    }

    getScore(winningDraw: number): number {
        return this.matrix.filter((f) => !f.drawn).reduce((a, c) => a + c.value, 0) * winningDraw;
    }

    protected equals(value1: { value: number; drawn: boolean }, value2: { value: number; drawn: boolean } | null | undefined): boolean {
        return value1.value === value2?.value;
    }

    private verifyBingoRow(rowIndex: number) {
        for (let i = 0; i < this.size.x; i++) {
            const { drawn } = this.get(i, rowIndex) ?? {};
            if (!drawn) {
                return false;
            }
        }
        this.hasBingo = true;
        return true;
    }

    private verifyBingoColumn(columnIndex: number) {
        for (let i = 0; i < this.size.y; i++) {
            const { drawn } = this.get(columnIndex, i) ?? {};
            if (!drawn) {
                return false;
            }
        }
        this.hasBingo = true;
        return true;
    }
}
