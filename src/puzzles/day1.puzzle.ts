import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { bufferCount, count, filter, Observable, pairwise } from 'rxjs';
import { map } from 'rxjs/operators';
import { Puzzle } from '../utils/di/puzzle.decorator';

@Puzzle(1)
export class Day1Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsNumberEvent().pipe(
            pairwise(),
            filter(([prev, curr]) => curr > prev),
            count()
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getLineAsNumberEvent().pipe(
            bufferCount(3, 1),
            filter((result) => result.length === 3),
            map(([a, b, c]) => a + b + c),
            pairwise(),
            filter(([prev, curr]) => curr > prev),
            count()
        );
    }
}
