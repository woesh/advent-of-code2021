import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Puzzle(7)
export class Day7Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ splitChar: /,/gm }).pipe(
            map((v) => v.map((s) => parseInt(s))),
            switchMap((positions) => of([positions, median(positions)] as [Array<number>, number])),
            map(([positions, bestPos]) => positions.map((pos) => Math.abs(pos - bestPos)).reduce((a, c) => a + c, 0))
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ splitChar: /,/gm }).pipe(
            map((v) => v.map((s) => parseInt(s))),
            switchMap((positions) => of([positions, average(positions)] as [Array<number>, number])),
            map(([positions, bestPos]) => {
                positions.map((pos) => computeCost(pos, bestPos)).reduce((a, c) => a + c, 0);

                return positions.map((pos) => computeCost(pos, bestPos)).reduce((a, c) => a + c, 0);
            })
            // map((v) => Math.round(v))
        );
    }
}

function median(numbers: Array<number>, k?: number): number {
    if (numbers.length > 2 && !isEqual(numbers)) {
        k = k ?? Math.ceil(numbers.length / 2);

        const pivot = numbers[Math.floor(Math.random() * (numbers.length - 1))];
        const [lesser, greater] = [numbers.filter((n) => n <= pivot), numbers.filter((n) => n > pivot)];

        return k < lesser.length ? median(lesser, k) : median(greater, k - lesser.length);
    }
    return numbers[0];
}

function average(positions: Array<number>) {
    return Math.round(positions.reduce((a, c) => a + c, 0) / positions.length);
}

function isEqual(numbers: Array<number>) {
    return numbers.every((v) => v === numbers[0]);
}

function computeCost(pos: number, bestPos: number) {
    return (Math.abs(pos - bestPos) * (1 + Math.abs(pos - bestPos))) / 2;
}
