import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce } from 'rxjs';
import { Coordinates } from '../utils/generic/coordinates.interface';
import { map } from 'rxjs/operators';

@Puzzle(13)
export class Day13Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce(
                ([foldable, size], lines) => {
                    const c = /(\w+),(\w+)/gm.exec(lines);
                    if (c !== null) {
                        foldable.add(parseInt(c[1]), parseInt(c[2]));
                    }
                    const f = /fold along ([yx])=(\w+)/gm.exec(lines);
                    if (f !== null) {
                        foldable.fold(f[1] as 'x' | 'y', parseInt(f[2]));
                        size = size !== 0 ? size : foldable.positions.size;
                    }
                    return [foldable, size];
                },
                [new Foldable(), 0] as any
            ),
            map(([, size]) => size)
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((foldable, lines) => {
                const c = /(\w+),(\w+)/gm.exec(lines);
                if (c !== null) {
                    foldable.add(parseInt(c[1]), parseInt(c[2]));
                }
                const f = /fold along ([yx])=(\w+)/gm.exec(lines);
                if (f !== null) {
                    foldable.fold(f[1] as 'x' | 'y', parseInt(f[2]));
                }
                return foldable;
            }, new Foldable()),
            map((f: Foldable) => f.print())
        );
    }
}

class Foldable {
    positions: Map<string, number>;

    constructor() {
        this.positions = new Map();
    }

    add(x: number, y: number, dots?: number): void {
        const current = this.positions.get(FoldableKey.key(x, y)) ?? 0;
        this.positions.set(FoldableKey.key(x, y), current + (dots ?? 1));
    }

    fold(axis: 'x' | 'y', line: number) {
        const foldedPoints = this.find(axis, line);
        if (axis === 'x') {
            foldedPoints.forEach(([c, dots]) => {
                const key = FoldableKey.fromKey(c);
                this.positions.delete(c);
                this.add(line - Math.abs(line - key.x), key.y, dots);
            });
        }

        if (axis === 'y') {
            foldedPoints.forEach(([c, dots]) => {
                const key = FoldableKey.fromKey(c);
                this.positions.delete(c);
                this.add(key.x, line - Math.abs(line - key.y), dots);
            });
        }
    }

    find(axis: 'x' | 'y', line: number) {
        return Array.from(this.positions.entries()).filter(([c]) => {
            const key = FoldableKey.fromKey(c);
            return axis === 'y' ? key.y > line : key.x > line;
        });
    }

    print() {
        let points = Array.from(this.positions.keys())
            .map((s) => FoldableKey.fromKey(s))
            .sort((k1, k2) => {
                return k1.x === k2.x ? k1.y - k2.y : k1.x - k2.x;
            })
            .reduce((a, c) => {
                if (!a[c.y]) a[c.y] = [];
                a[c.y][c.x] = '█';
                return a;
            }, [] as Array<Array<any>>);

        let print = '';

        for (let y = 0; y < points.length; y++) {
            print += '\r\n';
            for (let x = 0; x < points[y].length; x++) {
                print += points[y][x] ? points[y][x] : ' ';
            }
        }
        return print;
    }
}

class FoldableKey implements Coordinates {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    static fromKey(key: string) {
        const parsedKey: Array<number> = key.split(',').map((v) => parseInt(v));
        return new FoldableKey(parsedKey.at(0) as number, parsedKey.at(1) as number);
    }

    static key(x: number, y: number): string {
        return new FoldableKey(x, y).toKey();
    }

    toKey(): string {
        return `${this.x},${this.y}`;
    }
}
