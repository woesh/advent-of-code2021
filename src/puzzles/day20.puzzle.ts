import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { InfiniteMatrix } from '../utils/generic/matrix.class';
import { Coordinates } from '../utils/generic/coordinates.interface';

@Puzzle(20)
export class Day20Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: /\r?\n\r?\n/gm }).pipe(
            map(([algorithm, pixels]) => {
                const image = new Image(algorithm, '.');
                pixels
                    .split(/\r?\n/gm)
                    .map((p) => [...p])
                    .forEach((p, y) => {
                        p.forEach((v, x) => image.set({ x, y }, v as Pixel));
                    });
                return image;
            }),
            map((i) => {
                let image = i;
                for (let j = 0; j < 2; j++) {
                    image = image.output();
                }
                return image;
            }),
            map((i) => i.lightPixels)
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: /\r?\n\r?\n/gm }).pipe(
            map(([algorithm, pixels]) => {
                const image = new Image(algorithm, '.');
                pixels
                    .split(/\r?\n/gm)
                    .map((p) => [...p])
                    .forEach((p, y) => {
                        p.forEach((v, x) => image.set({ x, y }, v as Pixel));
                    });
                return image;
            }),
            map((i) => {
                let image = i;
                for (let j = 0; j < 50; j++) {
                    image = image.output();
                }
                return image;
            }),
            map((i) => i.lightPixels)
        );
    }
}

class Image extends InfiniteMatrix<Pixel> {
    algorithm: string;
    dimensions: { min: Coordinates; max: Coordinates };
    infinitePixel: Pixel;

    constructor(algorithm: string, infinitePixel: Pixel) {
        super();
        this.algorithm = algorithm;
        this.dimensions = { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } };
        this.infinitePixel = infinitePixel;
    }

    get value() {
        return this.matrix;
    }

    get lightPixels() {
        return Array.from(this.matrix.values()).filter((v) => v === '#').length;
    }

    set({ x, y }: Coordinates, value: Pixel) {
        const { min, max } = this.dimensions;
        min.x = min.x > x ? x : min.x;
        min.y = min.y > y ? y : min.y;
        max.x = max.x < x ? x : max.x;
        max.y = max.y < y ? y : max.y;
        super.set({ x, y }, value);
    }

    getAlgorithmValue(binary: string): Pixel {
        return this.algorithm.at(parseInt(binary, 2)) as Pixel;
    }

    enhancePixel({ x, y }: Coordinates) {
        const result = [-1, 0, 1].map((dy) => {
            return [-1, 0, 1].map((dx) => {
                return this.get({ x: dx + x, y: dy + y }) ?? this.infinitePixel;
            });
        });
        const binary = result.map((p) => p.map((v) => (v === '.' ? 0 : 1)).join('')).join('');
        return this.getAlgorithmValue(binary);
    }

    output() {
        const output = new Image(this.algorithm, this.getAlgorithmValue(new Array(10).join(this.infinitePixel === '.' ? '0' : '1')));

        for (let x = this.dimensions.min.x - 1; x <= this.dimensions.max.x + 1; x++) {
            for (let y = this.dimensions.min.y - 1; y <= this.dimensions.max.y + 1; y++) {
                const value1 = this.enhancePixel({ x, y });
                output.set({ x: x + 1, y: y + 1 }, value1);
            }
        }
        return output;
    }
}

type Pixel = '#' | '.';
