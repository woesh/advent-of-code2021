import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { filter, Observable, OperatorFunction, reduce } from 'rxjs';
import { map } from 'rxjs/operators';
import { Puzzle } from '../utils/di/puzzle.decorator';

@Puzzle(2)
export class Day2Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent().pipe(
            map((val) => /(\w+)\s(\d+)/.exec(val)),
            filter((val) => val !== null) as OperatorFunction<any, RegExpExecArray>,
            reduce(
                (acc, [, direction, distance]) => {
                    return parseDirection(acc, direction, parseInt(distance));
                },
                [0, 0]
            ),
            map(([hPos, vPos]) => hPos * vPos)
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent().pipe(
            map((val) => /(\w+)\s(\d+)/.exec(val)),
            filter((val) => val !== null) as OperatorFunction<any, RegExpExecArray>,
            reduce(
                (acc, [, direction, distance]) => {
                    return parseAimDirection(acc, direction, parseInt(distance));
                },
                [0, 0, 0]
            ),
            map(([hPos, vPos]) => hPos * vPos)
        );
    }
}

function parseDirection(coords: any, direction: string, distance: number): any {
    const [hPos, vPos] = coords;
    switch (direction) {
        case 'up':
            return [hPos, vPos - distance];
        case 'down':
            return [hPos, vPos + distance];
        case 'forward':
            return [hPos + distance, vPos];
    }
}

function parseAimDirection(coords: any, direction: string, distance: number): any {
    const [hPos, vPos, aim] = coords;
    switch (direction) {
        case 'up':
            return [hPos, vPos, aim - distance];
        case 'down':
            return [hPos, vPos, aim + distance];
        case 'forward':
            return [hPos + distance, vPos + aim * distance, aim];
    }
}
