import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import PriorityQueue from 'ts-priority-queue/src/PriorityQueue';

@Puzzle(23)
export class Day23Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLines(false).pipe(
            map(parseInput),
            map((input) => {
                const burrow = new AmphipodBurrow(2);
                return burrow.calculate(input);
            })
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLines(false).pipe(
            map(parseInput2),
            map((input) => {
                const burrow = new AmphipodBurrow(4);
                return burrow.calculate(input);
            })
        );
    }
}

const ROOM_WALL_OFFSET = 7;
const NR_OF_TYPES = 4;

interface Amphipod {
    type: string;
    pos: number;
}

class AmphipodBurrow {
    private readonly roomSize: number;

    constructor(roomSize: number) {
        this.roomSize = roomSize;
    }

    calculate(startingPositions: Array<Amphipod>) {
        const brQueue = new PriorityQueue<BurrowRecord>({ comparator: (a, b) => a.cost - b.cost });
        brQueue.queue(
            new BurrowRecord(
                startingPositions.map((o) => o.pos),
                0
            )
        );

        let leastEnergy = Infinity;
        const cache = new Map<string, number>();

        while (brQueue.length > 0) {
            const burrowRecord = brQueue.dequeue()!;
            if (burrowRecord.cost >= leastEnergy) {
                break;
            }

            burrowRecord.amphipods.forEach((amphipod, index) => {
                const validPos = this.findValidPositions(burrowRecord.amphipods, index);
                for (let i = 0; i < validPos.length; i++) {
                    if (!validPos[i]) {
                        continue;
                    }
                    const newBurrowRecord = burrowRecord.next(index, i, this.cost(index, amphipod, i));
                    if (newBurrowRecord.isComplete()) {
                        leastEnergy = Math.min(leastEnergy, newBurrowRecord.cost);
                    } else {
                        const key = newBurrowRecord.toString();
                        if (newBurrowRecord.cost < (cache.get(key) ?? Infinity)) {
                            cache.set(key, newBurrowRecord.cost);
                            brQueue.queue(newBurrowRecord);
                        }
                    }
                }
            });
        }
        return leastEnergy;
    }

    findValidRoomPositions(positions: Array<number>, unit: number): Array<boolean> {
        const occupied = new Array(positions.length + ROOM_WALL_OFFSET).fill(-1);
        positions.forEach((p, i) => (occupied[p] = i));
        const validRooms = new Array<boolean>(this.totalUnits() + ROOM_WALL_OFFSET);

        let cPos = positions[unit];
        let type = this.getType(unit);
        let room = type + ROOM_WALL_OFFSET;

        if (!this.checkHallwayClear(cPos, room, occupied)) {
            return validRooms;
        }
        let tgt = room;
        for (let i = 0; i < this.roomSize; i++) {
            if (occupied[room + NR_OF_TYPES * i] == -1) {
                tgt = room + NR_OF_TYPES * i;
            } else if (this.getType(occupied[room + NR_OF_TYPES * i]) != type) {
                return validRooms;
            }
        }
        validRooms[tgt] = true;
        return validRooms;
    }

    findValidHallPositions(positions: Array<number>, unit: number) {
        const occupied = new Array(positions.length + ROOM_WALL_OFFSET).fill(-1);
        positions.forEach((p, i) => (occupied[p] = i));

        const validHalls: Array<boolean> = new Array<boolean>(ROOM_WALL_OFFSET);

        let cPos = positions[unit];
        let type = this.getType(unit);

        for (let i = cPos - NR_OF_TYPES; i > 6; i -= NR_OF_TYPES) {
            if (occupied[i] > -1) {
                return validHalls;
            }
        }

        if ((cPos + 1) % NR_OF_TYPES == type) {
            let moving = false;
            for (let i = cPos + NR_OF_TYPES; i < this.totalUnits() + ROOM_WALL_OFFSET; i += NR_OF_TYPES) {
                if (this.getType(occupied[i]) != type) {
                    moving = true;
                    break;
                }
            }
            if (!moving) {
                return validHalls;
            }
        }

        let effPos = cPos;
        while (effPos > 10) {
            effPos -= NR_OF_TYPES;
        }
        for (let i = 0; i < ROOM_WALL_OFFSET; i++) {
            if (occupied[i] == -1 && this.checkHallwayClear(i, effPos, occupied)) {
                validHalls[i] = true;
            }
        }
        return validHalls;
    }

    checkHallwayClear(hallPos: number, roomPos: number, occupied: Array<number>) {
        let min = Math.min(hallPos + 1, roomPos - 5);
        let max = Math.max(hallPos - 1, roomPos - 6);

        for (let i = min; i <= max; i++) {
            if (occupied[i] != -1) {
                return false;
            }
        }
        return true;
    }

    findValidPositions(positions: Array<number>, unit: number) {
        if (positions[unit] < ROOM_WALL_OFFSET) {
            return this.findValidRoomPositions(positions, unit);
        } else {
            return this.findValidHallPositions(positions, unit);
        }
    }

    cost(unit: number, from: number, to: number) {
        if (from > to) {
            [from, to] = [to, from];
        }
        const distance = Math.abs(2 * from - (((to + 1) % NR_OF_TYPES) * 2 + 3)) + Math.floor((to - 3) / NR_OF_TYPES) - (from == 0 || from == 6 ? 1 : 0);
        return Math.pow(10, this.getType(unit)) * distance;
    }

    private totalUnits(): number {
        return NR_OF_TYPES * this.roomSize;
    }

    private getType(unit: number): number {
        return unit == -1 ? -1 : Math.floor(unit / this.roomSize);
    }
}

class BurrowRecord {
    private readonly _amphipods: Array<number>;
    private readonly _cost: number;

    constructor(positions: Array<number>, cost: number) {
        this._amphipods = positions;
        this._cost = cost;
    }

    get cost(): number {
        return this._cost;
    }

    get amphipods(): Array<number> {
        return this._amphipods;
    }

    next(unit: number, position: number, price: number) {
        const newPositions = [...this.amphipods];
        newPositions[unit] = position;
        return new BurrowRecord(newPositions, this.cost + price);
    }

    isComplete() {
        return !this._amphipods.some((p, i) => p < ROOM_WALL_OFFSET || (p + 1) % NR_OF_TYPES != Math.floor(i / (this.amphipods.length / NR_OF_TYPES)));
    }

    toString() {
        const occupied = new Array(this.amphipods.length + ROOM_WALL_OFFSET).fill(-1);
        this.amphipods.forEach((p, i) => (occupied[p] = i));
        return occupied
            .map((o) => {
                const type = Math.floor(o / (this.amphipods.length / NR_OF_TYPES));
                return type === -1 ? '.' : type;
            })
            .join('');
    }
}

function parseInput(input: Array<string>): Array<Amphipod> {
    return [...input[2].split(/#*([A-D])#*/gm), ...input[3].split(/#*([A-D])#*/gm)]
        .map((s) => s.trim())
        .filter(Boolean)
        .map((type, pos) => ({ type: type, pos: pos + ROOM_WALL_OFFSET }))
        .sort((a, b) => a.type.localeCompare(b.type));
}

function parseInput2(input: Array<string>): Array<Amphipod> {
    return [
        ...input[2].split(/#*([A-D])#*/gm),
        ...'#D#C#B#A#'.split(/#*([A-D])#*/gm),
        ...'#D#B#A#C#'.split(/#*([A-D])#*/gm),
        ...input[3].split(/#*([A-D])#*/gm),
    ]
        .map((s) => s.trim())
        .filter(Boolean)
        .map((type, pos) => ({ type, pos: pos + ROOM_WALL_OFFSET }))
        .sort((a, b) => a.type.localeCompare(b.type));
}
