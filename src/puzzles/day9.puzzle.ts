import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, of, reduce } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { DynamicMatrix } from '../utils/generic/matrix.class';
import { Coordinates } from '../utils/generic/coordinates.interface';

@Puzzle(9)
export class Day9Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((acc, inputString, index) => {
                acc.changeDimensions(inputString.length, index + 1);
                inputString
                    .split(/\s*/gm)
                    .map(Number)
                    .forEach((n) => acc.add(n));
                return acc;
            }, new HeightMap()),
            map((hm) => hm.findLowPoints().map((l) => hm.get(l.x, l.y) as number)),
            map((lowPoints) => lowPoints.length + lowPoints.reduce((a, c) => a + c), 0)
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((acc, inputString, index) => {
                acc.changeDimensions(inputString.length, index + 1);
                inputString
                    .split(/\s*/gm)
                    .map(Number)
                    .forEach((n) => acc.add({ value: n }));
                return acc;
            }, new BasinMap()),
            switchMap((hm) =>
                of(
                    ...hm
                        .findLowPoints()
                        .map((lp) => hm.getBasin(lp))
                        .sort((a, b) => b - a)
                        .splice(0, 3)
                )
            ),
            reduce((total, basins) => total * basins, 1)
        );
    }
}

class HeightMap<T> extends DynamicMatrix<number> {
    findLowPoints(): Array<Coordinates> {
        return this.matrix
            .map((_, i) => {
                return this.isLowPoint(this.index(i)) ? this.index(i) : null;
            })
            .filter((v) => v !== null) as Array<Coordinates>;
    }

    isLowPoint(coordinates: Coordinates) {
        const { x, y } = coordinates;
        return [this.get(x - 1, y), this.get(x + 1, y), this.get(x, y - 1), this.get(x, y + 1)]
            .filter((v) => v !== null)
            .every((h) => {
                const node = this.get(x, y);
                return h !== null && node !== null ? h > node : false;
            });
    }
}

class BasinMap extends DynamicMatrix<BasinNode> {
    findLowPoints(): Array<Coordinates> {
        return this.matrix
            .map((_, i) => {
                return this.isLowPoint(this.index(i)) ? this.index(i) : null;
            })
            .filter((v) => v !== null) as Array<Coordinates>;
    }

    isLowPoint(coordinates: Coordinates): boolean {
        const { x, y } = coordinates;
        return [this.get(x - 1, y), this.get(x + 1, y), this.get(x, y - 1), this.get(x, y + 1)]
            .filter((v) => v !== null)
            .every((h) => {
                const node = this.get(x, y);
                return h !== null && node !== null ? h.value > node.value : false;
            });
    }

    getBasin(lowPoint: Coordinates) {
        return this.getBasinNeighbourCount(lowPoint);
    }

    private getBasinNeighbourCount(c: Coordinates): number {
        const node = this.get(c.x, c.y);
        if (node !== null) {
            this.set(c.x, c.y, { value: node.value, visited: true });
            return (
                1 +
                (this.testNode({ x: c.x - 1, y: c.y }) ? this.getBasinNeighbourCount({ x: c.x - 1, y: c.y }) : 0) +
                (this.testNode({ x: c.x, y: c.y - 1 }) ? this.getBasinNeighbourCount({ x: c.x, y: c.y - 1 }) : 0) +
                (this.testNode({ x: c.x + 1, y: c.y }) ? this.getBasinNeighbourCount({ x: c.x + 1, y: c.y }) : 0) +
                (this.testNode({ x: c.x, y: c.y + 1 }) ? this.getBasinNeighbourCount({ x: c.x, y: c.y + 1 }) : 0)
            );
        }
        return 0;
    }

    private testNode(c: Coordinates) {
        const node = this.get(c.x, c.y);
        return node && !node.visited && node.value !== 9;
    }
}

interface BasinNode {
    value: number;
    visited?: boolean;
}
