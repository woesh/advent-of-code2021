import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { map, Observable } from 'rxjs';

@Puzzle(6)
export class Day6Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: /,/gm }).pipe(
            map((state) => state.map(Number)),
            map((state) => {
                const lanternFish = new LanternFish(state);
                for (let i = 0; i < 80; i++) {
                    lanternFish.progressDay();
                }
                return lanternFish.total;
            })
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: /,/gm }).pipe(
            map((state) => state.map(Number)),
            map((state) => {
                const lanternFish = new LanternFish(state);
                for (let i = 0; i < 256; i++) {
                    lanternFish.progressDay();
                }
                return lanternFish.total;
            })
        );
    }
}

class LanternFish {
    fishPerAge: Map<number, number> = new Map<number, number>();

    constructor(populate: Array<number>) {
        populate.forEach((age) => this.fishPerAge.set(age, (this.fishPerAge.get(age) ?? 0) + 1));
    }

    get total() {
        return Array.from(this.fishPerAge.values()).reduce((a, c) => a + c, 0);
    }

    progressDay() {
        const amount = this.fishPerAge.get(0) ?? 0;
        this.updateAge(8);
        this.fishPerAge.set(6, (this.fishPerAge.get(6) ?? 0) + amount);
    }

    updateAge(age: number) {
        const amount = this.fishPerAge.get(age) ?? 0;

        if (age > 0) {
            this.updateAge(age - 1);
            this.fishPerAge.set(age - 1, amount);
        } else {
            this.fishPerAge.set(8, amount);
        }
    }
}
