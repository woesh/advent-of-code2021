import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(21)
export class Day21Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((g, l) => {
                const [, label, start] = /Player (\d+) starting position: (\d+)/gm.exec(l)!;
                g.addPlayer(label, parseInt(start) - 1);
                return g;
            }, new DiracDiceGame(100, 1000)),
            map((game) => {
                while (!game.hasWinner) {
                    game.doTurn();
                }
                return game.turn * 3 * (game.losingPlayer?.score ?? 0);
            })
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((g, l) => {
                const [, label, start] = /Player (\d+) starting position: (\d+)/gm.exec(l)!;
                g.addPlayer(label, parseInt(start));
                return g;
            }, new DiracDiceGame(3, 21)),
            map((game) => {
                return doRecursiveTurn(game.players[0].location - 1, game.players[1].location - 1, 0, 0);
            }),
            map((wins) => Math.max(...wins))
        );
    }
}

const cache: Record<string, [number, number]> = {};

class DiracDiceGame {
    dice: Dice;
    players: Array<Player> = [];
    turn: number;
    winningScore: number;

    constructor(diceSides: number, winningScore: number) {
        this.winningScore = winningScore;
        this.dice = new Dice(diceSides);
        this.turn = 0;
    }

    get hasWinner(): boolean {
        return this.players.some((p) => p.score >= this.winningScore);
    }

    get losingPlayer(): Player | undefined {
        return this.players.find((p) => p.score < this.winningScore);
    }

    addPlayer(label: string, start: number) {
        this.players.push(new Player(label, start));
    }

    doTurn() {
        const p = this.getPlayerByTurn();
        const totalRoll = this.dice.rollTurn();
        p.location = (p.location + totalRoll) % 10;
        p.score += p.location + 1;
        this.turn++;
    }

    private getPlayerByTurn(): Player {
        return this.players[this.turn % this.players.length];
    }
}

class Dice {
    private index = 1;
    private readonly sides: number;

    constructor(sides: number) {
        this.sides = sides;
    }

    rollTurn() {
        return Array(this.roll(), this.roll(), this.roll()).reduce((a, c) => a + c, 0);
    }

    private roll(): number {
        const result = this.index;
        if (this.index < this.sides) {
            this.index++;
        } else {
            this.index = 1;
        }
        return result;
    }
}

class Player {
    label: string;
    location: number;
    score: number;
    wins: number;

    constructor(label: string, location: number) {
        this.label = label;
        this.location = location;
        this.score = 0;
        this.wins = 0;
    }
}

function quantumDice() {
    const quantumPossibilities: Array<[number, number, number]> = [];
    for (let i = 1; i <= 3; i++) {
        for (let j = 1; j <= 3; j++) {
            for (let k = 1; k <= 3; k++) {
                quantumPossibilities.push([i, j, k]);
            }
        }
    }
    return quantumPossibilities;
}

function cachedRecursiveTurn(turnFn: Function): Function {
    return (pawnOne: number, pawnTwo: number, score1: number, score2: number) => {
        const cacheKey = `${pawnOne}:${pawnTwo}:${score1}:${score2}`;
        if (cache[cacheKey] !== undefined) {
            return cache[cacheKey];
        }
        const result = turnFn(pawnOne, pawnTwo, score1, score2);
        cache[cacheKey] = result;
        return result;
    };
}

function doRecursiveTurn(activePlayerLocation: number, secondaryPlayerLocation: number, activePlayerScore: number, secondaryPlayerScore: number) {
    if (activePlayerScore >= 21) return [1, 0];
    if (secondaryPlayerScore >= 21) return [0, 1];
    let totalP1Wins = 0;
    let totalP2Wins = 0;
    for (const [rollOne, rollTwo, rollThree] of quantumDice()) {
        const newPawnOne = (activePlayerLocation + rollOne + rollTwo + rollThree) % 10;
        const newScoreOne = activePlayerScore + newPawnOne + 1;
        const [p2Wins, p1Wins] = cachedRecursiveTurn(doRecursiveTurn)(secondaryPlayerLocation, newPawnOne, secondaryPlayerScore, newScoreOne);
        totalP1Wins = totalP1Wins + p1Wins;
        totalP2Wins = totalP2Wins + p2Wins;
    }
    return [totalP1Wins, totalP2Wins];
}
