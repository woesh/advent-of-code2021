import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { interval, Observable, reduce, take, tap } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { loop } from '../utils/generic/observable.util';

@Puzzle(14)
export class Day14Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((pt, lines) => {
                const reaction = /([A-Z]{2}) -> ([A-Z])/gm.exec(lines);
                if (reaction) {
                    pt.addReaction([...reaction[1]] as [string, string], reaction[2]);
                } else {
                    [...lines].forEach((e) => pt.add(e));
                }
                return pt;
            }, new PolymerTemplate()),
            switchMap((pt) =>
                loop(10, () => {
                    pt.synthesize();
                }).pipe(map(() => pt))
            ),
            map((pt) => pt.countElements()),
            map((count) => {
                const sort = Object.values(count).sort((a, b) => b - a);
                return (sort.at(0) ?? 0) - (sort.at(-1) ?? 0);
            })
        );
    }
    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((pt, lines) => {
                const reaction = /([A-Z]{2}) -> ([A-Z])/gm.exec(lines);
                if (reaction) {
                    pt.addReaction([...reaction[1]] as [string, string], reaction[2]);
                } else {
                    [...lines].forEach((e) => pt.add(e));
                }
                return pt;
            }, new PolymerTemplate()),
            switchMap((pt) =>
                loop(1, () => {
                    pt.synthesize();
                }).pipe(map(() => pt))
            ),
            map((pt) => pt.countElements()),
            map((count) => {
                const sort = Object.values(count).sort((a, b) => b - a);
                return (sort.at(0) ?? 0) - (sort.at(-1) ?? 0);
            })
        );
    }
}

class PolymerTemplate {
    polymer: LinkedList<Element> | undefined;
    reactions: Array<Reaction>;

    constructor() {
        this.reactions = [];
    }

    addReaction(pair: [string, string], result: string) {
        this.reactions.push(new Reaction(pair, result));
    }

    add(e: string) {
        const element = new Element(e);
        if (this.polymer) {
            this.polymer.add(element);
        } else {
            this.polymer = new LinkedList<Element>(element);
        }
    }

    synthesize() {
        let pair = this.pair();
        while (pair) {
            const [first, second] = pair;
            const newElement = this.getReactionResult(first.item.element, second.item.element);

            first.insert(newElement);
            pair = this.pair(second);
        }
        return this;
    }

    pair(ll?: LinkedList<Element>): Array<LinkedList<Element>> | null {
        if (ll) {
            if (!ll.isLastElement()) return [ll, ll.next!];
            return null;
        }
        return this.pair(this.polymer);
    }

    getReactionResult(first: string, second: string): Element {
        return new Element(this.reactions.find((r) => r.elements[0] === first && r.elements[1] === second)!.result);
    }

    countElements() {
        const count: { [keyof: string]: number } = {};

        let ll = this.polymer;
        while (ll) {
            count[ll.item.element] = (count[ll.item.element] ?? 0) + 1;

            ll = ll.next;
        }

        return count;
    }
}

class Element {
    element: string;

    constructor(element: string) {
        this.element = element;
    }
}

class Reaction {
    elements: [string, string];
    result: string;

    constructor(elements: [string, string], result: string) {
        this.elements = elements;
        this.result = result;
    }
}

class LinkedList<T> {
    public next?: LinkedList<T>;
    private readonly _item: T;

    constructor(item: T) {
        this._item = item;
    }

    get last(): LinkedList<T> {
        if (this.next) {
            return this.next.last;
        }
        return this;
    }

    get item(): T {
        return this._item;
    }

    add(element: T) {
        if (!this.next) {
            this.next = new LinkedList<T>(element);
        } else {
            this.last.add(element);
        }
    }

    insert(item: T) {
        const ll = new LinkedList<T>(item);
        if (this.next) {
            ll.next = this.next;
            this.next = ll;
        }
    }

    getAll(): Array<T> {
        return [this._item, ...(this.next?.getAll() ?? [])];
    }

    isLastElement() {
        return !this.next;
    }
}
