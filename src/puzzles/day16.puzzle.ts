import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce, tap } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(16)
export class Day16Puzzle extends BasePuzzle {
    // part1(): Observable<any> {
    //     return this.inputParser.getCharacterAsStringEvent(true).pipe(
    //         map(toBinary),
    //         reduce((a, c) => a + c),
    //         map((binary) => {
    //             const b = new Binary(binary);
    //             console.log(b);
    //             return start(b);
    //         }),
    //         tap((a) => console.log(JSON.stringify(a)))
    //     );
    // }
}

// class Binary {
//     value: string;
//
//     constructor(binary: string) {
//         this.value = binary;
//     }
// }
//
// class Packet {
//     version: number;
//     typeId: number;
//
//     literalValue: number | undefined;
//
//     subPackets: Array<Packet>;
//
//     constructor(version: number, typeID: number) {
//         this.version = version;
//         this.typeId = typeID;
//         this.subPackets = [];
//     }
//
//     getTotalVersions(): number {
//         return this.subPackets.reduce((a, c) => {
//             a += c.getTotalVersions();
//             return a;
//         }, this.version);
//     }
// }
//
// type Bit = '0' | '1';
//
// function read(binary: Binary, length: number): number {
//     const result = toDecimal(binary.value.substring(0, length));
//     binary.value = binary.value.substring(length);
//     return result;
// }
//
// function start(binary: Binary) {
//     const version = read(binary, Bits.Version);
//     const typeId = read(binary, Bits.TypeId);
//
//     console.log(version, typeId, binary);
// }
//
// function recursive(binary: string) {}
//
// function parsePacket(binary: string): OperatorResult | LiteralResult {
//     const version = toDecimal(binary.substring(0, 3));
//     const typeId = toDecimal(binary.substring(3, 6));
//     console.log(`Packet v`, version, `t`, typeId);
//     const packet = new Packet(version, typeId);
//
//     return parseBinary(packet, binary.substring(6));
// }
//
// function parseBinary(packet: Packet, binary: string): any {
//     let result;
//     switch (packet.typeId) {
//         case 4:
//             result = parseLiteral(packet, binary);
//             break;
//         default:
//             result = parseOperator(packet, binary);
//             break;
//     }
//     return result;
// }
//
// function parseLiteral(packet: Packet, binary: string): OperatorResult {
//     let subBinary = '',
//         length = 0;
//     while (binary.charAt(0) !== '0') {
//         subBinary += binary.substring(1, 5);
//         binary = binary.substring(5);
//         length += 5;
//     }
//     subBinary += binary.substring(1, 5);
//     length += 5;
//     console.log('Literal', [toDecimal(subBinary), length]);
//     packet.literalValue = toDecimal(subBinary);
//     return [packet, length];
// }
//
// function parseOperator(packet: Packet, binary: string) {
//     const lengthBit = binary.substring(0, 1) as Bit;
//     if (lengthBit === '0') {
//         const length = toDecimal(binary.substring(1, 16));
//         console.log('operator bit', lengthBit, '#length', length);
//         let subBinary = binary.substring(16, 16 + length);
//         while (subBinary.length > 0) {
//             const items = start(subBinary);
//             packet.subPackets.push(items[0] as Packet);
//             subBinary = subBinary.substring(items[1] + 6);
//         }
//         return [packet, length];
//     }
//     const packets = toDecimal(binary.substring(1, 12));
//     console.log('operator bit', lengthBit, '#packets', packets);
//
//     return;
// }
//
// type LiteralResult = [number, number];
// type OperatorResult = [Packet, number];
//
// enum Bits {
//     Version = 3,
//     TypeId = 3,
//     LiteralGroupHeader = 1,
//     LiteralGroupLength = 4,
//     LengthTypeID = 1,
//     TotalSubpacketBitLength = 15,
//     SubpacketCount = 11,
// }
//
// function toBinary(hex: string) {
//     return parseInt(hex, 16).toString(2).padStart(4, '0');
// }
//
// function toDecimal(binary: string) {
//     return parseInt(binary, 2);
// }
