import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { InfiniteMatrix } from '../utils/generic/matrix.class';
import { Coordinates } from '../utils/generic/coordinates.interface';

@Puzzle(25)
export class Day25Puzzle extends BasePuzzle {}
