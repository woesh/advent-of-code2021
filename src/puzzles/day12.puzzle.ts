import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(12)
export class Day12Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((cs, line) => {
                const caves = line.split('-').map((name) => cs.getCave(name));
                caves[0].addNeighbour(caves[1]);
                caves[1].addNeighbour(caves[0]);
                return cs;
            }, new CaveSystem()),
            map((cs) => {
                return cs.getRoutes();
            })
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((cs, line) => {
                const caves = line.split('-').map((name) => cs.getCave(name));
                caves[0].addNeighbour(caves[1]);
                caves[1].addNeighbour(caves[0]);
                return cs;
            }, new CaveSystem()),
            map((cs) => {
                return cs.getRoutes(true);
            })
        );
    }
}

class CaveSystem {
    caves: Array<Cave>;

    constructor() {
        this.caves = [];
    }

    private static parse(caveName: string): Cave {
        if (/[A-Z]+/.test(caveName)) {
            return new BigCave(caveName);
        }
        return new SmallCave(caveName);
    }

    getCave(name: string): Cave {
        let cave = this.caves.find((c) => c.name === name);
        if (!cave) {
            cave = CaveSystem.parse(name);
            this.caves.push(cave);
        }
        return cave;
    }

    getRoutes(allowDuplicate?: boolean) {
        const start = this.getCave('start');
        return start.neighbours
            .map((neighbourCave) => {
                allowDuplicate = allowDuplicate ?? false;
                const visited = [];
                visited.push(start);
                return this.getRoute([...visited], neighbourCave, allowDuplicate);
            })
            .reduce((a, c) => a + c, 0);
    }

    getRoute(visited: Array<Cave>, from: Cave, allowDuplicate: boolean): number {
        if (from.name !== 'end') {
            if (from instanceof SmallCave) {
                if (visited.includes(from)) {
                    allowDuplicate = false;
                }
                visited.push(from);
            }
            return from.neighbours
                .filter((nb) => {
                    if (!allowDuplicate) {
                        return !visited.includes(nb);
                    } else {
                        return true;
                    }
                })
                .filter((nb) => nb.name !== 'start')
                .map((cave) => {
                    return this.getRoute([...visited], cave, allowDuplicate);
                })
                .reduce((a, c) => a + c, 0);
        }
        return 1;
    }
}

abstract class Cave {
    name: string;
    neighbours: Array<Cave>;

    protected constructor(name: string) {
        this.name = name;
        this.neighbours = [];
    }

    addNeighbour(cave: Cave) {
        this.neighbours.push(cave);
    }
}

class BigCave extends Cave {
    constructor(name: string) {
        super(name);
    }
}

class SmallCave extends Cave {
    constructor(name: string) {
        super(name);
    }
}
