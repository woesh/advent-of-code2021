import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(18)
export class Day18Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((input) => JSON.parse(input)),
            reduce((a, c) => {
                return a.push(c), a;
            }, [] as SnailFishNotation),
            map((input) => {
                const tree = new FishTree(input[0]);
                for (let i = 1; i < input.length; i++) {
                    tree.addArray(input[i]).reduce();
                }
                return tree.magnitude();
            })
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((input) => JSON.parse(input)),
            reduce((a, c) => {
                return a.push(c), a;
            }, [] as SnailFishNotation),
            map((input) => {
                let maxMag = 0;
                for (let j = 0; j < input.length; j++)
                    for (let i = 0; i < input.length; i++) {
                        const tree = new FishTree(input[j]);
                        tree.addArray(input[i]).reduce();
                        maxMag = Math.max(maxMag, tree.magnitude());
                    }
                return maxMag;
            })
        );
    }
}

type SnailFishNotation = Array<SnailFishNotation & number>;

class FishTree {
    fishTree: Array<any>;
    rootId: number;

    constructor(sourceArray: SnailFishNotation) {
        this.fishTree = [];
        this.rootId = 0;
        this.fromArray(sourceArray);
    }

    node(params: any): number {
        return this.fishTree.push({ ...params, id: this.fishTree.length }) - 1;
    }

    magnitude(n = this.rootId): any {
        return this.fishTree[n].val !== undefined
            ? this.fishTree[n].val
            : this.magnitude(this.fishTree[n].left) * 3 + this.magnitude(this.fishTree[n].right) * 2;
    }

    fromArray(params: SnailFishNotation | number, parentId?: any, id?: any) {
        id ??= false;
        if (Array.isArray(params)) {
            id = this.node({ left: 0, right: 0, parentId });
            ['left', 'right'].map((dir, i) => (this.fishTree[id][dir] = this.fromArray(params[i], id)));
        } else id = this.node({ val: params, parentId });
        return id;
    }

    addArray(arr: SnailFishNotation) {
        let newRootId = this.node({ left: this.rootId, right: false });
        this.fishTree[this.rootId].parentId = newRootId;
        this.fishTree[newRootId].right = this.fromArray(arr, newRootId);
        this.rootId = newRootId;
        return this;
    }

    reduce() {
        const reducible = (n: number, depth: number, condition: string) => {
            let res: boolean | number = false;
            if (condition == 'DEPTH') {
                if (depth == 4 && this.fishTree[n].val === undefined) return n;
            } else {
                if (this.fishTree[n].val != undefined) return this.fishTree[n].val > 9 ? n : false;
            }
            ['left', 'right']
                .filter((dir) => this.fishTree[n][dir] !== undefined)
                .some((dir) => {
                    res = reducible(this.fishTree[n][dir], depth + 1, condition);
                    if (res) return true;
                });
            return res;
        };

        const traverseNode = (node: number) => {
            // finds left/right target for traversal
            const traverse = (n: number, dir: string, opDir: string = dir == 'left' ? 'right' : 'left') => {
                let parentId = this.fishTree[n].parentId;
                while (parentId !== undefined && this.fishTree[parentId][dir] == n) {
                    n = parentId;
                    parentId = this.fishTree[n].parentId;
                }
                if (parentId == undefined) return -1;
                n = this.fishTree[parentId][dir];
                while (this.fishTree[n][opDir]) {
                    n = this.fishTree[n][opDir];
                }
                return n;
            };

            ['left', 'right']
                .filter((dir) => traverse(node, dir) !== -1)
                .map((dir) => (this.fishTree[traverse(node, dir)].val += this.fishTree[this.fishTree[node][dir]].val));
            this.fishTree[node] = { id: node, parentId: this.fishTree[node].parentId, val: 0 };
        };

        const splitNode = (node: number) => {
            let val = this.fishTree[node].val / 2;
            this.fishTree[node] = {
                id: this.fishTree[node].id,
                parentId: this.fishTree[node].parentId,
                left: this.node({ parentId: node, val: Math.floor(val) }),
                right: this.node({ parentId: node, val: Math.ceil(val) }),
            };
        };

        const step = () => {
            let id = reducible(this.rootId, 0, 'DEPTH');
            if (id !== false) return traverseNode(id);
            id = reducible(this.rootId, 0, 'SIZE');
            if (id !== false) return splitNode(id);
            return false;
        };

        while (step() !== false) {}
    }
}
