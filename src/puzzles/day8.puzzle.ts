import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of, reduce } from 'rxjs';

@Puzzle(8)
export class Day8Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            switchMap((line) => of(...line.split(/\|\s*/gm)[1].split(/\s/gm))),
            reduce((acc, value) => {
                return [2, 3, 4, 7].includes(value.length) ? acc + 1 : acc;
            }, 0)
        );
    }

    part2(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((line) => line.split(/\|\s*/gm)),
            switchMap(([pattern, digits]) =>
                of([
                    pattern
                        .split(/\s/gm)
                        .filter((p) => p !== '')
                        .map((p) => p.split('').sort() as Array<Segment>),
                    digits
                        .split(/\s/gm)
                        .filter((p) => p !== '')
                        .map((d) => d.split('').sort() as Array<Segment>),
                ])
            ),
            switchMap(([patterns, digits]) => {
                const cipher: Array<number> = new Array(10);

                cipher[1] = patterns.findIndex((p) => p.length === 2);
                cipher[4] = patterns.findIndex((p) => p.length === 4);
                cipher[7] = patterns.findIndex((p) => p.length === 3);
                cipher[8] = patterns.findIndex((p) => p.length === 7);
                cipher[3] = patterns.findIndex((p) => p.length === 5 && patterns[cipher[7]].every((x) => p.includes(x)));
                cipher[9] = patterns.findIndex((p) => p.length === 6 && patterns[cipher[4]].every((x) => p.includes(x)));
                cipher[5] = patterns.findIndex((p, i) => p.length === 5 && p.every((x) => patterns[cipher[9]].includes(x)) && !cipher.includes(i));
                cipher[2] = patterns.findIndex((p, i) => p.length === 5 && !cipher.includes(i));
                cipher[6] = patterns.findIndex((p, i) => p.length === 6 && patterns[cipher[5]].every((x) => p.includes(x)) && !cipher.includes(i));
                cipher[0] = patterns.findIndex((p, i) => p.length === 6 && !cipher.includes(i));

                const decipher = cipher.map((index) => patterns[index].join(''));
                return of([decipher, digits.map((d) => d.join(''))]);
            }),
            reduce((acc, [decipher, digits]) => {
                return acc + parseInt(digits.map((d) => decipher.findIndex((i) => i === d)).join(''));
            }, 0)
        );
    }
}

type Segment = 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g';
