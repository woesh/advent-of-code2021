import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, reduce } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(22)
export class Day22Puzzle extends BasePuzzle {
    // part1(): Observable<any> {
    //     return this.inputParser.getLineAsStringEvent(false).pipe(
    //         map((line) => {
    //             const [state, coords] = line.split(/\s/);
    //             return [state, coords.split(',').map((s) => s.split(/([xyz])=(-?\d+)\.\.(-?\d+),?/gm).filter(Boolean))];
    //         }),
    //         map((input) => {
    //             const cuboid: any = { state: input[0] };
    //             for (let c of input[1]) cuboid[c[0]] = { start: parseInt(c[1]), end: parseInt(c[2]) };
    //             return cuboid as Cuboid;
    //         }),
    //         reduce((cubes, cuboid) => {
    //             return bruteForce(cubes, cuboid);
    //         }, new Set() as Set<string>),
    //         map((cubes) => cubes.size)
    //     );
    // }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(true).pipe(
            map((line) => {
                const [state, coords] = line.split(/\s/);
                return [state, coords.split(',').map((s) => s.split(/([xyz])=(-?\d+)\.\.(-?\d+),?/gm).filter(Boolean))];
            }),
            map((input) => {
                const cuboid: any = { state: input[0] };
                for (let c of input[1]) cuboid[c[0]] = { start: parseInt(c[1]), end: parseInt(c[2]) };
                return cuboid as Cuboid;
            }),
            reduce((cs, c) => {
                const cuboids = process(cs, c);
                return cs.push(...cuboids), cs;
            }, [] as Array<Cuboid>),
            map((cuboids) => cuboids.reduce((a, c) => a + volume(c), 0n)),
            map((bigInt) => bigInt.toString())
        );
    }
}

function containing(existing: Cuboid, newCuboid: Cuboid) {
    return (
        existing.x.start <= newCuboid.x.start &&
        existing.x.end >= newCuboid.x.end &&
        existing.y.start <= newCuboid.y.start &&
        existing.y.end >= newCuboid.y.end &&
        existing.z.start <= newCuboid.z.start &&
        existing.z.end >= newCuboid.z.end
    );
}

function process(cuboids: Array<Cuboid>, newCuboid: Cuboid): Array<Cuboid> {
    //Check if cuboid is fully contained by an existing cuboid, ignore new if yes
    if (cuboids.some((c) => containing(c, newCuboid))) {
        return cuboids;
    }

    cuboids
        .filter((c) => intersects(c, newCuboid))
        .map((c) => {
            // Has an intersect with a(n) existing cuboid(s)
            return subtract(c, newCuboid);
        })
        .flat();

    //Add new cuboid to the list
    if (newCuboid.state === 'on') {
        cuboids.push(newCuboid);
    }

    return cuboids;
}

function subtract(e: Cuboid, n: Cuboid) {
    console.log('x', e.x, n.x);
    console.log('y', e.y, n.y);
    console.log('z', e.z, n.z);
    console.log('----');

    return [e];
}

function intersects(a: Cuboid, b: Cuboid): boolean {
    return a.x.start <= b.x.end && a.x.end >= b.x.start && a.y.start <= b.y.end && a.y.end >= b.y.start && a.z.start <= b.z.end && a.z.end >= b.z.start;
}

function volume(cuboid: Cuboid) {
    // return bi(cuboid.x.end - cuboid.x.start) * bi(cuboid.y.end - cuboid.y.start) * bi(cuboid.z.end - cuboid.z.start);
    return bi((cuboid.x.end - cuboid.x.start) * (cuboid.y.end - cuboid.y.start) * (cuboid.z.end - cuboid.z.start));
}

function bi(a: number | bigint): bigint {
    return BigInt(a);
}

interface Cuboid {
    state: 'on' | 'off';
    x: CuboidRange;
    y: CuboidRange;
    z: CuboidRange;
}

interface CuboidRange {
    start: number;
    end: number;
}

function bruteForce(cubes: Set<string>, cuboid: Cuboid) {
    const { x, y, z } = cuboid;
    for (let dx = x.start; dx <= x.end; dx++) {
        if (dx >= -50 && dx <= 50) {
            for (let dy = y.start; dy <= y.end; dy++) {
                if (dy >= -50 && dy <= 50) {
                    for (let dz = z.start; dz <= z.end; dz++) {
                        if (dz >= -50 && dz <= 50) {
                            const key = dx + ':' + dy + ':' + dz;
                            cuboid.state === 'on' ? cubes.add(key) : cubes.delete(key);
                        }
                    }
                }
            }
        }
    }
    return cubes;
}
