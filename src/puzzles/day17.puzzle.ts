import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(17)
export class Day17Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent().pipe(
            map((input) =>
                /target area: x=(\d+)..(\d+), y=(-?\d+)..(-?\d+)/gm
                    .exec(input)!
                    .slice(1, 5)
                    .map((x) => parseInt(x))
            ),
            map(([x1, x2, y1, y2]) => {
                const limit = range(-200, 0).concat(range(200));
                return cartesian(limit, limit)
                    .map((result) => calculateTrajectory(x1, x2, y1, y2)(result))
                    .filter((p) => p.length !== 0);
            }),
            map((ys) => Math.max(...ys.flat()))
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent().pipe(
            map((input) =>
                /target area: x=(\d+)..(\d+), y=(-?\d+)..(-?\d+)/gm
                    .exec(input)!
                    .slice(1, 5)
                    .map((x) => parseInt(x))
            ),
            map(([x1, x2, y1, y2]) => {
                const limit = range(-250, 0).concat(range(250));
                return cartesian(limit, limit)
                    .map((result) => calculateTrajectory(x1, x2, y1, y2)(result))
                    .filter((p) => p.length !== 0);
            }),
            map((ys) => ys.length)
        );
    }
}

export const cartesian = (list1: Array<number>, list2: Array<number>): Array<number>[] => list1.flatMap((x) => list2.map((y) => [x, y]));

export const range = (a: number, b?: number, step = 1): number[] => {
    const size = Math.ceil(b !== undefined ? Math.abs(b - a) / Math.abs(step) : a / step);
    const offset = b !== undefined ? a : 0;
    return [...Array(size).keys()].map((i) => offset + i * step);
};

function calculateTrajectory(x1: number, x2: number, y1: number, y2: number) {
    return ([limitX, limitY]: Array<any>) => {
        const xs = [0],
            ys = [0];
        while (true) {
            const x = xs[xs.length - 1];
            const y = ys[ys.length - 1];

            if (x >= x1 && x <= x2 && y >= y1 && y <= y2) return ys;
            if (x > x2 || y < y1) return [];

            xs.push(x + limitX);
            ys.push(y + limitY);
            limitX = limitX > 0 ? limitX - 1 : limitX < 0 ? limitX + 1 : limitX;
            limitY = limitY - 1;
        }
    };
}
