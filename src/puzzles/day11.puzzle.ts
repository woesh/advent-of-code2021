import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable, of, reduce, switchMap } from 'rxjs';
import { Matrix } from '../utils/generic/matrix.class';
import { map } from 'rxjs/operators';

@Puzzle(11)
export class Day11Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: /\s*/gm }).pipe(
            switchMap((result) => of(...result.map((s) => parseInt(s)))),
            reduce((acc, val) => (acc.add({ energy: val }), acc), new Octopuses()),
            map((o) => {
                for (let i = 0; i < 100; i++) {
                    o.increaseLevel();
                    o.flashOctopus();
                    o.resetEnergyLevels();
                }
                return o.flashCounter;
            })
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: /\s*/gm }).pipe(
            switchMap((result) => of(...result.filter((s) => s.length).map((s) => parseInt(s)))),
            reduce((acc, val) => (acc.add({ energy: val }), acc), new Octopuses()),
            map((o) => {
                let stepCounter = 0;
                while (!o.synchronized) {
                    o.increaseLevel();
                    o.flashOctopus();
                    o.resetEnergyLevels();
                    stepCounter++;
                }
                return stepCounter;
            })
        );
    }
}

class Octopuses extends Matrix<Octopus> {
    flashCounter: number = 0;

    constructor() {
        super(10, 10);
    }

    get synchronized() {
        return this.matrix.every((o) => this.matrix[0].energy === o.energy);
    }

    increaseLevel() {
        this.setAll((o) => ({ ...o, energy: o.energy + 1 }));
    }

    flashOctopus() {
        while (this.matrix.filter((o) => o.energy > 9 && !o.flashed).length > 0) {
            this.matrix
                .filter((o) => o.energy > 9 && !o.flashed)
                .forEach((o) => {
                    o.flashed = true;
                    this.flashCounter++;
                    this.getNeighbours(o).forEach((n) => n.energy++);
                });
        }
    }

    resetEnergyLevels() {
        this.setAll((o) => ({ flashed: false, energy: o.energy > 9 ? 0 : o.energy }));
    }

    getNeighbours(octopus: Octopus): Array<Octopus> {
        const position = this.position(octopus);
        const neighbours = [];
        if (position !== null) {
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    const { x, y } = position;
                    const o = this.get(x + dx, y + dy);
                    neighbours.push(o);
                }
            }
            return neighbours.filter((o) => o !== null) as Array<Octopus>;
        }
        return [];
    }
}

interface Octopus {
    energy: number;
    flashed?: boolean;
}
