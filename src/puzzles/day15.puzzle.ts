import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { from, Observable, of, reduce, tap } from 'rxjs';
import { DynamicMatrix, NestedMatrix } from '../utils/generic/matrix.class';
import { map, switchMap } from 'rxjs/operators';
import { Coordinates } from '../utils/generic/coordinates.interface';

@Puzzle(15)
export class Day15Puzzle extends BasePuzzle {
    part1(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            reduce((m, line) => {
                m.size.x = line.length;
                m.size.y = (m.size.y ?? 0) + 1;
                [...line].forEach((w) => m.add(parseInt(w)));
                return m;
            }, new AdjacencyMatrix()),
            map((m) => m.shortestPath())
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getInputAsSplit({ testData: false, splitChar: '\n' }).pipe(
            map((input) =>
                input.map((row) =>
                    row
                        .split('')
                        .filter((v) => !isNaN(parseInt(v)))
                        .map(Number)
                )
            ),
            map((map) =>
                [...Array(map.length * 5)].map((_, y) =>
                    [...Array(map[0].length * 5)].map(
                        (_, x) => 1 + ((map[y % map.length][x % map[0].length] - 1 + Math.trunc(x / map[0].length) + Math.trunc(y / map.length)) % 9)
                    )
                )
            ),
            switchMap((map: NestedMatrix<number>) => of(...map)),
            reduce((m, line) => {
                m.size.x = line.length;
                m.size.y = (m.size.y ?? 0) + 1;
                line.forEach((w) => m.add(w));
                return m;
            }, new AdjacencyMatrix()),
            map((m) => m.shortestPath())
        );
    }
}

class AdjacencyMatrix extends DynamicMatrix<number> {
    shortestPath(startPos?: Coordinates) {
        startPos ??= { x: 0, y: 0 };
        const neighbours = [
            [1, 0],
            [0, 1],
            [-1, 0],
            [0, -1],
        ];
        const queue: Array<Queue> = [{ pos: startPos, cost: 0 }];
        const visited = new Set<string>();
        while (queue.length) {
            const {
                pos: { x, y },
                cost,
            } = queue.shift()!;
            if (y === this.size.y - 1 && x === this.size.x - 1) {
                return cost;
            } else {
                neighbours
                    .map(([dx, dy]) => [dx + x, dy + y])
                    .filter(([x, y]) => this.get(x, y) && !visited.has(`${x},${y}`))
                    .forEach(([x, y]) => {
                        visited.add(`${x},${y}`);
                        queue.push({ pos: { x, y }, cost: cost + this.get(x, y)! });
                    });
                queue.sort((a, b) => a.cost - b.cost);
            }
        }
    }
}

interface Queue {
    pos: Coordinates;
    cost: number;
}
