import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(7)
export class Day7BFPuzzle extends BasePuzzle {
    part2(): Observable<string | number> {
        return this.inputParser.getInputAsSplit({ splitChar: /,/gm }).pipe(
            map((v) => v.map((s) => parseInt(s))),
            map((input) => {
                let maxFuel: number | undefined;
                let max = Math.max(...input);

                for (let i = 0; i <= max; i++) {
                    let step;
                    const fuel = input.reduce(
                        (totalFuel, steps) => (
                            (step = steps > i ? steps - i : i - steps), totalFuel + (step % 2 === 0 ? (step / 2) * step + step / 2 : Math.ceil(step / 2) * step)
                        ),
                        0
                    );
                    if (!maxFuel || fuel < maxFuel) {
                        maxFuel = fuel;
                    }
                }

                return maxFuel ?? NaN;
            })
        );
    }
}
