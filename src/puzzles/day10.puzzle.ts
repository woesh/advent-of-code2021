import { BasePuzzle } from '../utils/puzzle/base-puzzle.class';
import { Puzzle } from '../utils/di/puzzle.decorator';
import { filter, Observable, OperatorFunction, reduce } from 'rxjs';
import { map } from 'rxjs/operators';

@Puzzle(10)
export class Day10Puzzle extends BasePuzzle {
    part1(): Observable<string | number> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((line) => [...line] as Array<SyntaxChar>),
            map((chars) => findInvalidChar(chars)),
            filter((invalid) => invalid !== null) as OperatorFunction<SyntaxChar | null, SyntaxChar>,
            map((invalid) => score(invalid)),
            reduce((a, c) => a + c, 0)
        );
    }

    part2(): Observable<any> {
        return this.inputParser.getLineAsStringEvent(false).pipe(
            map((line) => [...line] as Array<SyntaxChar>),
            filter((validLines) => findInvalidChar(validLines) === null),
            map((chars) => getNonClosedChars(chars)),
            map((chars) =>
                chars.reverse().reduce((score, char) => {
                    return score * 5 + score2(inverseChar(char, true));
                }, 0)
            ),
            reduce((acc, value) => (acc.push(value), acc), [] as Array<number>),
            map((scores) => scores.sort((a, b) => b - a).at(Math.floor(scores.length / 2)))
        );
    }
}

function findInvalidChar(chars: Array<SyntaxChar>): SyntaxChar | null {
    const stack: Array<SyntaxChar> = [];

    return chars.reduce((invalidChar, char) => {
        if ('([{<'.includes(char)) {
            stack.push(char);
        } else {
            if (stack.at(-1) === inverseChar(char)) {
                stack.pop();
            } else {
                return invalidChar === null ? char : invalidChar;
            }
        }
        return invalidChar;
    }, null as SyntaxChar | null);
}

function getNonClosedChars(chars: Array<SyntaxChar>): Array<SyntaxChar> {
    return chars.reduce((stack, char) => {
        if ('([{<'.includes(char)) {
            stack.push(char);
        } else {
            if (stack.at(-1) === inverseChar(char)) {
                stack.pop();
            }
        }
        return stack;
    }, [] as Array<SyntaxChar>);
}

function inverseChar(char: SyntaxChar, matchValue?: boolean): SyntaxChar {
    const inverse: SyntaxTable = {
        ')': '(',
        ']': '[',
        '}': '{',
        '>': '<',
    };
    return !matchValue ? inverse[char] : (Object.keys(inverse).find((key) => inverse[key] === char) as SyntaxChar);
}

function score(char: SyntaxChar) {
    const score: SyntaxScore = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137,
    };
    return score[char];
}

function score2(char: SyntaxChar | undefined) {
    const score: SyntaxScore = {
        ')': 1,
        ']': 2,
        '}': 3,
        '>': 4,
    };
    return score[char ?? ''];
}

type SyntaxChar = '(' | '[' | '{' | '<' | ')' | ']' | '}' | '>';
type SyntaxTable = { [key: string]: SyntaxChar };
type SyntaxScore = { [key: string]: number };
