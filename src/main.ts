import { BasePuzzle, PuzzleOutput } from './utils/puzzle/base-puzzle.class';
import { concat, last, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Service } from 'typedi';
import { PuzzleContainer } from './utils/di/puzzle.container';

@Service()
export class Main {
    private puzzles: Array<BasePuzzle> = [];

    constructor() {
        const all = PuzzleContainer.getAll();
        this.puzzles.push(
            ...all
                .sort((a, b) => a.day - b.day)
                .map((meta) => {
                    const puzzle = new meta.constructor(meta.day);
                    puzzle.day = meta.day;
                    return puzzle;
                })
        );
    }

    init(): Observable<any> {
        return of(0);
    }

    public run(): Observable<string> {
        return concat(
            ...this.puzzles
                .map((puzzle) => [
                    mapOutput(puzzle.constructor.name + ':1', puzzle.output(puzzle.part1())),
                    mapOutput(puzzle.constructor.name + ':2', puzzle.output(puzzle.part2())),
                ])
                .reduce((acc, val) => [...acc, ...val], [])
        );
    }
}

function mapOutput(name: string, source: Observable<PuzzleOutput>): Observable<string> {
    return source.pipe(
        last(),
        map((po) => {
            return `${name} @ ${po.duration}ms with answer: '${po.result}'`;
        })
    );
}
