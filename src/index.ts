import { Main } from './main';
import { switchMap } from 'rxjs/operators';
import { Container } from 'typedi';
import 'reflect-metadata';
import { PuzzleContainer } from './utils/di/puzzle.container';
import { Day1Puzzle } from './puzzles/day1.puzzle';
import { Day5Puzzle } from './puzzles/day5.puzzle';
import { Day8Puzzle } from './puzzles/day8.puzzle';
import { Day6Puzzle } from './puzzles/day6.puzzle';
import { Day9Puzzle } from './puzzles/day9.puzzle';
import { Day4Puzzle } from './puzzles/day4.puzzle';
import { Day2Puzzle } from './puzzles/day2.puzzle';
import { Day10Puzzle } from './puzzles/day10.puzzle';
import { Day7Puzzle } from './puzzles/day7.puzzle';
import { Day3Puzzle } from './puzzles/day3.puzzle';
import { Day7BFPuzzle } from './puzzles/day7_bf.puzzle';
import { Day11Puzzle } from './puzzles/day11.puzzle';
import { Day12Puzzle } from './puzzles/day12.puzzle';
import { Day13Puzzle } from './puzzles/day13.puzzle';
import { Day14Puzzle } from './puzzles/day14.puzzle';
import { Day15Puzzle } from './puzzles/day15.puzzle';
import { Day16Puzzle } from './puzzles/day16.puzzle';
import { Day18Puzzle } from './puzzles/day18.puzzle';
import { Day17Puzzle } from './puzzles/day17.puzzle';
import { Day19Puzzle } from './puzzles/day19.puzzle';
import { Day20Puzzle } from './puzzles/day20.puzzle';
import { Day22Puzzle } from './puzzles/day22.puzzle';
import { Day25Puzzle } from './puzzles/day25.puzzle';
import { Day24Puzzle } from './puzzles/day24.puzzle';
import { Day23Puzzle } from './puzzles/day23.puzzle';
import { Day21Puzzle } from './puzzles/day21.puzzle';

let main = Container.get<Main>(Main);

main.init()
    .pipe(
        switchMap(() => {
            console.log('Running Puzzles');

            // Execute puzzles
            return main.run();
        })
    )
    .subscribe(console.log);

PuzzleContainer.import([
    // Day1Puzzle,
    // Day2Puzzle,
    // Day3Puzzle,
    // Day4Puzzle,
    // Day5Puzzle,
    // Day6Puzzle,
    // Day7Puzzle,
    // Day7BFPuzzle,
    // Day8Puzzle,
    // Day9Puzzle,
    // Day10Puzzle,
    // Day11Puzzle,
    // Day12Puzzle,
    // Day13Puzzle,
    // Day14Puzzle,
    // Day15Puzzle,
    // Day16Puzzle,
    // Day17Puzzle,
    // Day18Puzzle,
    // Day19Puzzle,
    // Day20Puzzle,
    Day21Puzzle,
    Day22Puzzle,
    Day23Puzzle,
    Day24Puzzle,
    Day25Puzzle,
]);
