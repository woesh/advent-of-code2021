import { Constructable } from 'typedi';
import { BasePuzzle } from '../puzzle/base-puzzle.class';

export interface PuzzleMetaData {
    constructor: Constructable<BasePuzzle>;
    day: number;
}

export class PuzzleContainer {
    private registeredClasses: Array<PuzzleMetaData> = [];

    private constructor() {}

    private static _instance: PuzzleContainer;

    private static get instance() {
        if (!PuzzleContainer._instance) {
            PuzzleContainer._instance = new PuzzleContainer();
        }
        return PuzzleContainer._instance;
    }

    static add(metaData: PuzzleMetaData) {
        PuzzleContainer.instance.registeredClasses.push(metaData);
    }

    static getAll(): Array<PuzzleMetaData> {
        return PuzzleContainer.instance.registeredClasses;
    }

    static import(imports: any) {
        return true;
    }
}
