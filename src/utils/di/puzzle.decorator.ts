import { PuzzleContainer } from './puzzle.container';
import { Constructable } from 'typedi';
import { BasePuzzle } from '../puzzle/base-puzzle.class';

export function Puzzle(day: number) {
    return function (target: Constructable<BasePuzzle>) {
        PuzzleContainer.add({ constructor: target, day });
    };
}
