import axios from 'axios';
import { catchError, defer, from, Observable } from 'rxjs';
import { createWriteStream, promises as pfs } from 'fs';
import path from 'path';
import { switchMap } from 'rxjs/operators';
import { Service } from 'typedi';

const _PATH = '.cache/inputs';

@Service()
export class InputCache {
    test: InputTestCache;

    constructor() {
        this.test = new InputTestCache();
    }

    init(): Observable<void> {
        return from(pfs.mkdir(_PATH, { recursive: true }).then(() => void 0));
    }

    get(day: number): Observable<string> {
        return defer(() => readInput(`input_${day}`)).pipe(
            catchError((err, source) => {
                console.log(`Downloading new input: (Day: ${day})`);
                return from(this.downloadInput(`https://adventofcode.com/2021/day/${day}/input`, `input_${day}`)).pipe(switchMap(() => source));
            })
        );
    }

    downloadInput(fileUrl: string, outputLocationPath: string): Promise<void> {
        const writer = createWriteStream(path.join(_PATH, outputLocationPath));

        return axios({
            method: 'get',
            url: fileUrl,
            responseType: 'stream',
            headers: { Cookie: 'session=53616c7465645f5f620be0e0c10af77addf7a94b59f82657039cd6ee43972e5c460a09555803ff8237c04bcdcc1c116a' },
        }).then((response) => {
            return new Promise<void>((resolve, reject) => {
                response.data.pipe(writer);
                writer.on('error', (err) => {
                    writer.close();
                    reject(err);
                });
                writer.on('close', () => {
                    resolve();
                });
            });
        });
    }
}

class InputTestCache {
    get(day: number): Observable<string> {
        return from(readInput(`test/input_${day}`));
    }
}

function readInput(outputLocationPath: string): Promise<string> {
    return pfs.readFile(path.join(_PATH, outputLocationPath), { encoding: 'utf-8' });
}
