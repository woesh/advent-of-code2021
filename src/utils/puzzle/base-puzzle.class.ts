import { defer, Observable, of } from 'rxjs';
import { performance } from 'perf_hooks';
import { map, switchMap } from 'rxjs/operators';
import { InputParser } from './input-parser.class';

export interface PuzzleOutput {
    result: number | string;
    duration: number;
}

export abstract class BasePuzzle {
    protected inputParser!: InputParser;

    public set day(value: number) {
        this.inputParser = new InputParser(value);
    }

    public part1(): Observable<any> {
        return of('Not Yet Implemented');
    }

    public part2(): Observable<any> {
        return of('Not Yet Implemented');
    }

    public output(fn: Observable<any>): Observable<PuzzleOutput> {
        return defer(() => of(performance.now())).pipe(
            switchMap((startTime) =>
                fn.pipe(
                    map((result: any) => ({
                        result: typeof result === 'string' || typeof result === 'number' || Array.isArray(result) ? (result as any) : JSON.stringify(result),
                        duration: Math.round((performance.now() - startTime) * 100) / 100,
                    }))
                )
            )
        );
    }
}
