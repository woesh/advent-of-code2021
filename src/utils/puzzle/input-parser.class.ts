import { from, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { InputCache } from './input-cache.class';
import { Container } from 'typedi';

interface InputOptions {
    splitChar: string | RegExp;
    testData?: boolean;
}

export class InputParser {
    private readonly input: InputCache;

    public constructor(private readonly day: number) {
        this.input = Container.get(InputCache);
    }

    public getLines(testData?: boolean): Observable<Array<string>> {
        return this.getInputAsSplit({ testData, splitChar: /\n|\r\n/g }).pipe(
            map((input) => {
                return input.filter((line) => line.trim().length !== 0);
            })
        );
    }

    public getInputAsSplit(opts: InputOptions): Observable<Array<string>> {
        const { testData, splitChar } = opts;
        return this.getFullInput(testData).pipe(map((input) => input.split(splitChar)));
    }

    public getLineAsStringEvent(testData?: boolean): Observable<string> {
        return (!testData ? this.getInput() : this.getTestInput()).pipe(
            switchMap((input) => {
                let strings = input.split(/\n|\r\n/g).filter((line) => line.trim().length !== 0);
                return of(...strings);
            })
        );
    }

    public getLineAsNumberEvent(testData?: boolean): Observable<number> {
        return this.getLineAsStringEvent(testData).pipe(map((value) => parseInt(value)));
    }

    public getCharacterAsStringEvent(testData?: boolean) {
        return this.getInputAsSplit({ testData, splitChar: /\s*/gm }).pipe(switchMap((result) => of(...result)));
    }

    getFullInput(testData?: boolean): Observable<string> {
        return !testData ? this.getInput() : this.getTestInput();
    }

    private getInput(): Observable<string> {
        return from(this.input.get(this.day));
    }

    private getTestInput(): Observable<string> {
        return from(this.input.test.get(this.day));
    }
}
