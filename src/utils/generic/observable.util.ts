import { interval, Observable, take, tap } from 'rxjs';

export function loop(iterations: number, fn?: (i: number) => void): Observable<any> {
    return interval().pipe(
        take(iterations),
        tap((i) => (fn ? fn(i) : null))
    );
}
