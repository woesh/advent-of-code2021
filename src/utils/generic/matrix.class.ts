import { Coordinates } from './coordinates.interface';

export class Matrix<T> {
    public readonly size: Coordinates;
    protected readonly matrix: Array<T>;

    constructor(x: number, y: number) {
        this.matrix = [];
        this.size = { x, y };
    }

    get total() {
        return this.matrix.length;
    }

    add(value: T) {
        this.matrix.push(value);
    }

    set(x: number, y: number, value: T): void {
        this.matrix[x + y * this.size.x] = value;
    }

    setAll(fn: (value: T, i: number) => T) {
        for (let i = 0; i < this.matrix.length; i++) {
            this.matrix[i] = fn(this.matrix[i], i);
        }
    }

    get(x: number, y: number): T | null {
        if (x >= 0 && x < this.size.x && y >= 0 && y < this.size.y) {
            return this.matrix[x + y * this.size.x];
        }
        return null;
    }

    index(index: number): Coordinates {
        return { x: index % this.size.x, y: Math.floor(index / this.size.x) };
    }

    position(value: T | Partial<T>): Coordinates | null {
        const index = this.matrix.findIndex((v) => this.equals(v, value));
        if (index >= 0) {
            return this.index(index);
        }
        return null;
    }

    visualize(): string {
        const matrix = [];
        for (let i = 0; i < this.matrix.length; i = i + this.size.x) {
            matrix.push(this.matrix.slice(i, i + this.size.x).join(', '));
        }
        return matrix.join('\r\n');
    }

    nested(): NestedMatrix<T> {
        return this.matrix.reduce((a, c, i) => {
            return i % this.size.x === 0 ? a.concat([this.matrix.slice(i, i + this.size.x)]) : a;
        }, [] as NestedMatrix<T>);
    }

    protected equals(value1: T, value2: T | Partial<T> | null | undefined): boolean {
        return value1 === value2;
    }
}

export class DynamicMatrix<T> extends Matrix<T> {
    constructor() {
        super(0, 0);
    }

    public changeDimensions(x: number, y: number) {
        this.size.x = x;
        this.size.y = y;
    }
}

export class InfiniteMatrix<T> {
    protected matrix: Map<string, T> = new Map();

    public static fromKey(key: string): Coordinates {
        const [x, y] = key.split(':').map((v) => parseInt(v));
        return { x, y };
    }

    private static toKey(x: number, y: number) {
        return `${x}:${y}`;
    }

    get({ x, y }: Coordinates) {
        return this.matrix.get(InfiniteMatrix.toKey(x, y));
    }

    set({ x, y }: Coordinates, value: T): void {
        this.matrix.set(InfiniteMatrix.toKey(x, y), value);
    }
}

export type NestedMatrix<T> = Array<Array<T>>;
