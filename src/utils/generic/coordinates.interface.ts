export interface Coordinates {
    x: number;
    y: number;
}

export interface ThreeDimensionCoordinates extends Coordinates {
    z: number;
}

export type ArrayCoordinates<N extends number> = Tuple<number, 3>;

type Tuple<T, N extends number> = N extends N ? (number extends N ? T[] : _TupleOf<T, N, []>) : never;
type _TupleOf<T, N extends number, R extends unknown[]> = R['length'] extends N ? R : _TupleOf<T, N, [T, ...R]>;
